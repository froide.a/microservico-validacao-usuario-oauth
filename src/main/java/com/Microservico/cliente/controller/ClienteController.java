package com.Microservico.cliente.controller;

import com.Microservico.cliente.DTO.CadastroClienteRequest;
import com.Microservico.cliente.DTO.ClienteResponse;
import com.Microservico.cliente.models.Cliente;
import com.Microservico.cliente.service.ClienteService;
import com.Microservico.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteResponse cadastrarCliente(@RequestBody @Valid CadastroClienteRequest cadastroClienteRequest, @AuthenticationPrincipal Usuario usuario) {
        try {
            ClienteResponse clienteResponse = new ClienteResponse();
            Cliente cliente = clienteService.cadastrarCliente(
                    cadastroClienteRequest.converterParaCliente(cadastroClienteRequest));
            return clienteResponse.converterParaCadastroClienteResponse(cliente);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ClienteResponse consultarClientePorId(@PathVariable(name = "id") long id, @AuthenticationPrincipal Usuario usuario) {
                   Cliente cliente = clienteService.consultarClientePorId(id);
            ClienteResponse clienteResponse = new ClienteResponse();
            return clienteResponse.converterParaCadastroClienteResponse(cliente);

    }
}
