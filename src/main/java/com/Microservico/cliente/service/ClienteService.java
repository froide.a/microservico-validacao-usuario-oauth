package com.Microservico.cliente.service;

import com.Microservico.cliente.Consistencia.ClienteConsistencia;
import com.Microservico.cliente.DTO.ClienteResponse;
import com.Microservico.cliente.models.Cliente;
import com.Microservico.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente) {
        try {
            return clienteRepository.save(cliente);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao cadastrar o cliente: " +
                    e.getMessage(), e);
        }
    }


    public Cliente consultarClientePorId(long id) {

            Optional<Cliente> cliente = clienteRepository.findById(id);
            if (cliente.isPresent()) {
                return cliente.get();
            } else {
                throw new ClienteConsistencia();
                //throw new RuntimeException("Cliente não cadastrado - tratamento de exception.");
            }

    }
}
