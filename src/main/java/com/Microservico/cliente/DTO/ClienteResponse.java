package com.Microservico.cliente.DTO;

import com.Microservico.cliente.models.Cliente;

public class ClienteResponse {
    private long id;
    private String nome;

    public ClienteResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ClienteResponse converterParaCadastroClienteResponse(Cliente cliente) {
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getNome());
        return clienteResponse;
    }
}
