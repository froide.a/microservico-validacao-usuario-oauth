package com.Microservico.cliente;

import com.Microservico.ClienteDecoder.ClienteDecoder;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteConfiguration {

    @Bean
    public ErrorDecoder getClienteDecoder() {
        return new ClienteDecoder();
    }


}
